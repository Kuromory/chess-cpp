#include <QApplication>
#include "gamemanager.h"

GameManager *game;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    game = new GameManager();
    game->show();
    game->displayMainMenu();

    return a.exec();
}
