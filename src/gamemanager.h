#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include <QMainWindow>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "src/chessboard/chessboard.h"


#include "src/piece/chesspiece.h"
#include "src/piece/rook.h"
#include <src/piece/pawn.h>

class GameManager : public QGraphicsView{
    Q_OBJECT

public:
    explicit GameManager(QWidget *parent = nullptr);

    void drawDeadHolder(int x, int y, QColor color);
    void drawChessBoard();
    void displayDeadWhite();
    void displayDeadBlack();
    void placeInDeadPlace(ChessPiece *piece);


    //Scene Related
    void addToScene(QGraphicsItem *item);
    void removeFromScene(QGraphicsItem *item);

    //getters/setters
    ChessPiece *pieceToMove = nullptr;
    Rook *rookToMove = nullptr;
    Pawn *pawnToMove = nullptr;

    void setTurn( QString value);
    QString getTurn() ;
    void changeTurn();

     ChessBox *collection[8][8];
     QGraphicsTextItem *check;
     QList <ChessPiece *> alivePiece;
     void displayMainMenu();

     void gameOver();
     void removeAll();

    bool endGame = false;


public slots:
     void start();

private:
    QGraphicsScene *gameScene;
    QGraphicsTextItem *turnDisplay;
    QGraphicsRectItem * deadHolder;
    ChessBoard *chess;
    QList <QGraphicsItem *> listG;
    QList <ChessPiece *>whiteDead;
    QList <ChessPiece *>blackDead;
    QString turn;
};

#endif // GAMEMANAGER_H
