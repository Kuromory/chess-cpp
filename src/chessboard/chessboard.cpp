#include "chessboard.h"
#include "chessbox.h"
#include "src/gamemanager.h"
#include <QGraphicsRectItem>
#include <QDebug>

#include "src/piece/rook.h"
#include "src/piece/knight.h"
#include "src/piece/bishop.h"
#include "src/piece/queen.h"
#include "src/piece/king.h"
#include "src/piece/pawn.h"

extern GameManager *game;
ChessBoard::ChessBoard()
{
    setUpBlack();
    setUpWhite();
//    qDebug() << black[0]->pieceName;


}
void ChessBoard::drawBoxes(int x, int y){
    int SHIFT = 100;
    for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++)
            {
                ChessBox *box = new ChessBox();
                game->collection[i][j] = box;
                box->xLoc = i;
                box->yLoc = j;
                box->setPos(x+SHIFT*i,y+SHIFT*j);
                if((i+j)%2==0)
                    box->setOriginalColor(Qt::lightGray);
                else
                    box->setOriginalColor(Qt::darkGray);

                game->addToScene(box);
            }
        }
}
void ChessBoard::addChessPiece(){
    for(int i = 0; i < 8; i++) {
           for(int j = 0; j < 8; j++)
           {
               ChessBox *box = game->collection[j][i];
               if(i < 2) {
                   static int k = 0;
                   box->placePiece(black[k]);
                   game->alivePiece.append(black[k]);
                   game->addToScene(black[k++]);
               }
               if(i > 5) {
                   static int h = 0;
                   box->placePiece(white[h]);
                   game->alivePiece.append(white[h]);
                   game->addToScene(white[h++]);
               }

           }
       }
}
void ChessBoard::setUpBlack(){
    ChessPiece *piece;
    Rook *rook;
    Rook *rook1;

    rook = new Rook("BLACK");
    black.append(rook);
    piece = new Knight("BLACK");
    black.append(piece);
    piece = new Bishop("BLACK");
    black.append(piece);
    piece = new Queen("BLACK");
    black.append(piece);
    rook1 = new Rook("BLACK");
    piece = new King("BLACK",rook,rook1);
    black.append(piece);
    piece = new Bishop("BLACK");
    black.append(piece);
    piece = new Knight("BLACK");
    black.append(piece);
    black.append(rook1);
    for(int i = 0; i < 8; i++) {
        piece = new Pawn("BLACK");
        black.append(piece);
    }
}

void ChessBoard::setUpWhite(){
    ChessPiece *piece;
    Rook *rook;
    Rook *rook1;
    for(int i = 0; i < 8; i++) {
        piece = new Pawn("WHITE");
        white.append(piece);
    }
    rook = new Rook("WHITE");
    white.append(rook);
    piece = new Knight("WHITE");
    white.append(piece);
    piece = new Bishop("WHITE");
    white.append(piece);
    piece = new Queen("WHITE");
    white.append(piece);
    rook1 = new Rook("WHITE");
    piece = new King("WHITE", rook, rook1);
    white.append(piece);
    piece = new Bishop("WHITE");
    white.append(piece);
    piece = new Knight("WHITE");
    white.append(piece);
    white.append(rook1);
}

void ChessBoard::reset() {
    int k = 0;
    int h = 0;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j <8 ; ++j) {
            ChessBox *box  = game->collection[j][i];
            box->setHasChessPiece(false);
            box->setChessPieceColor("NONE");
            box->currentPiece = nullptr;
            box->resetOriginalColor();
            if (i<2){
                box->placePiece(black[k]);
                black[k]->setIsPlaced(true);
                black[k]->firstMove = true;
                game->alivePiece.append(black[k++]);
            }
            if (i>5){
                box->placePiece(white[h]);
                white[h]->setIsPlaced(true);
                white[h]->firstMove = true;
                game->alivePiece.append(white[h++]);
            }
        }
    }
}
