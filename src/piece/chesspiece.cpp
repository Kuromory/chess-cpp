#include "chesspiece.h"
#include "src/gamemanager.h"
#include <QDebug>
#include "src/piece/king.h"
#include "src/chessboard/chessbox.h"

extern GameManager *game;

ChessPiece::ChessPiece(QString team, QGraphicsItem *parent) : QGraphicsPixmapItem(parent) {
    side = team;
    isPlaced = true;
    firstMove = true;
}

void ChessPiece::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //Deselect
    if (this == game->pieceToMove) {
        game->pieceToMove->getCurrentBox()->resetOriginalColor();
        game->pieceToMove->decolor();
        game->pieceToMove = nullptr;
        return;
    }
    if ((!getIsPlaced()) || ((game->getTurn() != this->getSide()) && (!game->pieceToMove))) {
        return;
    }
    if (!game->pieceToMove) {
        game->pieceToMove = this;
        game->pieceToMove->getCurrentBox()->setColor(Qt::red);
        game->pieceToMove->possibleMoves();
        if(game->rookToMove){
            game->rookToMove->castling();
        }


    } else if (this->getSide() != game->pieceToMove->getSide()) {
        this->getCurrentBox()->mousePressEvent(event);
    }
}

QString ChessPiece::getSide() {
    return side;

}

void ChessPiece::setIsPlaced(bool value) {
    isPlaced = value;
}

bool ChessPiece::getIsPlaced() {
    return isPlaced;
}

QList<ChessBox *> ChessPiece::getLocation() {
    return location;
}

void ChessPiece::setCurrentBox(ChessBox *box) {
    currentBox = box;
}

ChessBox *ChessPiece::getCurrentBox() {
    return currentBox;
}

void ChessPiece::setImage() {
    if (side == "WHITE") {
        setPixmap(QPixmap(":/images/" + pieceName + "1.png"));

    } else {
        setPixmap(QPixmap(":/images/" + pieceName + ".png"));
    }
}

void ChessPiece::decolor() {

    for (int i = 0, n = location.size(); i < n; i++) {
        location[i]->resetOriginalColor();
    }
}

bool ChessPiece::boxSetting(ChessBox *box) {
    if (box->getHasChessPiece()) {
        King *q = dynamic_cast<King *>(location.last()->currentPiece);
        if (q) {
            box->setColor(Qt::blue);
        } else {
            box->setColor(Qt::red);
        }
        return true;
    } else {
        location.last()->setColor(Qt::darkRed);
    }
    return false;
}
QString ChessPiece::getPieceName() {
    return pieceName;
}
