#include "knight.h"
#include "src/gamemanager.h"
#include "src/chessboard/chessbox.h"


extern GameManager *game;

Knight::Knight(QString team, QGraphicsItem *parent) : ChessPiece(team, parent) {
    pieceName = "horse";
    setImage();
}

void Knight::possibleMoves() {

    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;
    int i, j;

    QString team = this->getSide();

    //up up left
    i = x - 1;
    j = y - 2;
    if (i >= 0 && j >= 0 && (game->collection[i][j]->getChessPieceColor() != team)) {
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //up up right
    i = x + 1;
    j = y - 2;
    if (i <= 7 && j >= 0 && (game->collection[i][j]->getChessPieceColor() != team)) {
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //down down left
    i = x - 1;
    j = y + 2;
    if (i >= 0 && j <= 7 && (game->collection[i][j]->getChessPieceColor() != team)) {
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //down down right
    i = x + 1;
    j = y + 2;
    if (i <= 7 && j <= 7 && (game->collection[i][j]->getChessPieceColor() != team)) {
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //lef left up
    i = x -2;
    j = y -1;
    if (i>=0 && j>=0 && (game->collection[i][j]->getChessPieceColor() != team)){
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //lef left down
    i = x -2;
    j = y +1;
    if (i>=0 && j<=7 && (game->collection[i][j]->getChessPieceColor() != team)){
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //right right up
    i = x +2;
    j = y-1;
    if (i<=7 && j>=0 && (game->collection[i][j]->getChessPieceColor() != team)){
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }
    //right right up
    i = x +2;
    j = y+1;
    if (i<=7 && j<=7 && (game->collection[i][j]->getChessPieceColor() != team)){
        location.append(game->collection[i][j]);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        } else {
            location.last()->setColor(Qt::darkRed);
        }
    }


}
