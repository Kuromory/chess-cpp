#include "king.h"
#include "src/gamemanager.h"
#include <QDebug>
#include "src/chessboard/chessbox.h"

extern GameManager *game;

King::King(QString team, Rook *rook, Rook *rook1, QGraphicsItem *parent) : ChessPiece(team, parent) {
    pieceName = "king";
    this->rook = rook;
    this->rook1 = rook1;
    setImage();
}

void King::possibleMoves() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    QString team = this->getSide();


    //up
    if (y > 0 && !(game->collection[x][y - 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x][y - 1]);
        game->collection[x][y - 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //down
    if (y < 7 && !(game->collection[x][y + 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x][y + 1]);
        game->collection[x][y + 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }

    //left
    if (x > 0 && !(game->collection[x - 1][y]->getChessPieceColor() == team)) {
        location.append(game->collection[x - 1][y]);
        game->collection[x - 1][y]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //down left
    if (x > 0 && y < 7 && !(game->collection[x - 1][y + 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x - 1][y + 1]);
        game->collection[x - 1][y + 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //upper left
    if (x > 0 && y > 0 && !(game->collection[x - 1][y - 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x - 1][y - 1]);
        game->collection[x - 1][y - 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //right
    if (x < 7 && !(game->collection[x + 1][y]->getChessPieceColor() == team)) {
        location.append(game->collection[x + 1][y]);
        game->collection[x + 1][y]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //down right
    if (x < 7 && y < 7 && !(game->collection[x + 1][y + 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x + 1][y + 1]);
        game->collection[x + 1][y + 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }
    //upper right
    if (x < 7 && y > 0 && !(game->collection[x + 1][y - 1]->getChessPieceColor() == team)) {
        location.append(game->collection[x + 1][y - 1]);
        game->collection[x + 1][y - 1]->setColor(Qt::darkRed);
        if (location.last()->getHasChessPiece()) {
            location.last()->setColor(Qt::red);
        }
    }


        //right
        for (int i = 1; i <= 2; i++) {
            if (game->collection[x + i][y]->getHasChessPiece()) {
                break;
            }else if (i==2 && firstMove && rook1->firstMove) {
                game->collection[x + 2][y]->setColor(Qt::yellow);
                location.append(game->collection[x + 2][y]);
                game->rookToMove = rook1;
                game->rookToMove->direction = "RIGHT";
            }
        }

    //left
    for (int i = 1; i <= 2; i++) {
        if (game->collection[x - i][y]->getHasChessPiece()) {
            break;
        }else if (i==2 && firstMove && rook->firstMove) {
            game->collection[x - 2][y]->setColor(Qt::yellow);
            location.append(game->collection[x - 2][y]);
            game->rookToMove = rook;
            game->rookToMove->direction = "LEFT";
        }
    }
}



