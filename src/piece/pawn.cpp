#include "pawn.h"
#include "src/gamemanager.h"
#include "src/chessboard/chessbox.h"
#include <QDebug>

extern GameManager *game;

Pawn::Pawn(QString team, QGraphicsItem *parent) : ChessPiece(team, parent) {
    pieceName = "pawn";
    setImage();

}

void Pawn::possibleMoves() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    if (this->getSide() == "WHITE") {

        if (x < 7 && game->collection[x + 1][y]->getHasChessPiece() &&
            game->pawnToMove != nullptr) {
            if (game->collection[x + 1][y]->currentPiece == game->pawnToMove) {
                movedTwoSpaces();
            }
        }
        if (x > 0 && game->collection[x - 1][y]->getHasChessPiece() &&
            game->pawnToMove != nullptr) {
            if (game->collection[x - 1][y]->currentPiece == game->pawnToMove) {
                movedTwoSpaces();
            }
        }
        //left
        if (x > 0 && y > 0 && game->collection[x - 1][y - 1]->getChessPieceColor() == "BLACK") {
            location.append(game->collection[x - 1][y - 1]);
            boxSetting(location.last());
        }
        //right
        if (x < 7 && y > 0 && game->collection[x + 1][y - 1]->getChessPieceColor() == "BLACK") {
            location.append(game->collection[x + 1][y - 1]);
            boxSetting(location.last());
        }
        if (x <= 7 && (!game->collection[x][y - 1]->getHasChessPiece())) {
            location.append(game->collection[x][y - 1]);
            boxSetting(location.last());
            if (this->firstMove && !game->collection[x][y - 2]->getHasChessPiece()) {
                location.append(game->collection[x][y - 2]);
                boxSetting(location.last());
            }
        }
    } else {
        if (x < 7 && game->collection[x + 1][y]->getHasChessPiece() &&
            game->pawnToMove != nullptr) {
            if (game->collection[x + 1][y]->currentPiece == game->pawnToMove) {
                movedTwoSpaces();
            }
        }
        if (x > 0 && game->collection[x - 1][y]->getHasChessPiece() &&
            game->pawnToMove != nullptr) {
            if (game->collection[x - 1][y]->currentPiece == game->pawnToMove) {
                movedTwoSpaces();
            }
        }
        //left
        if (x > 0 && y < 7 && game->collection[x - 1][y + 1]->getChessPieceColor() == "WHITE") {
            location.append(game->collection[x - 1][y + 1]);
            boxSetting(location.last());
        }
        //right
        if (x < 7 && y < 7 && game->collection[x + 1][y + 1]->getChessPieceColor() == "WHITE") {
            location.append(game->collection[x + 1][y + 1]);
            boxSetting(location.last());
        }
        if (x <= 7 && (!game->collection[x][y + 1]->getHasChessPiece())) {
            location.append(game->collection[x][y + 1]);
            boxSetting(location.last());
            if (this->firstMove && !game->collection[x][y + 2]->getHasChessPiece()) {
                location.append(game->collection[x][y + 2]);
                boxSetting(location.last());
            }

        }
    }


}

void Pawn::movedTwoSpaces() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    if (this->getSide() == "WHITE") {
        if (game->pawnToMove->direction == "LEFT") {
            location.append(game->collection[x - 1][y - 1]);
            boxSetting(location.last());
        } else {
            location.append(game->collection[x + 1][y - 1]);
            boxSetting(location.last());
        }
    } else {
        if (game->pawnToMove->direction == "LEFT") {
            location.append(game->collection[x - 1][y + 1]);
            boxSetting(location.last());
        } else {
            location.append(game->collection[x + 1][y + 1]);
            boxSetting(location.last());
        }
    }


}

