#include "bishop.h"
#include "src/gamemanager.h"
#include "src/chessboard/chessbox.h"

extern GameManager *game;

Bishop::Bishop(QString team, QGraphicsItem *parent) : ChessPiece(team, parent) {
    pieceName = "bishop";
    setImage();
}

void Bishop::possibleMoves() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    QString team = this->getSide();

    //for down right to upper left
    for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }
    }

    //For down left to upper right

    for(int i = x+1,j = y-1; i <= 7 && j >= 0; i++,j--) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }
        }
    }
    //For upper left to down right

    for(int i = x+1,j = y+1; i <= 7 && j <= 7; i++,j++) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }
        }
    }
    //For upper right to down left

    for(int i = x-1,j = y+1; i >= 0 && j <= 7; i--,j++) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }

        }
    }

}
