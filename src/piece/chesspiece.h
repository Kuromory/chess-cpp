#ifndef CHESSPIECE_H
#define CHESSPIECE_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

class ChessBox;
class ChessPiece:public QGraphicsPixmapItem
{
public:
    ChessPiece(QString team = "",QGraphicsItem *parent = nullptr);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    void setCurrentBox(ChessBox *box);
    ChessBox *getCurrentBox();

    void setSide(QString value);
    QString getSide();

    void setIsPlaced(bool value);
    bool getIsPlaced();

    QList <ChessBox *> getLocation();

    QString pieceName;
    bool firstMove;
    void setImage();
    virtual void possibleMoves() = 0;

    void decolor();
    bool boxSetting(ChessBox *box);
    QString getPieceName();
    QList <ChessBox *> location;

protected:
    ChessBox *currentBox;
    QString side;
    bool isPlaced;


};

#endif // CHESSPIECE_H
